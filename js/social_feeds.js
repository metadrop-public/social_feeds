/**
 * @file
 * JavaScript file for archery forms.
 */

(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.social_feeds = {
    attach: function (context, settings) {

      if (typeof(Drupal.settings.social_feeds) != 'undefined' && Drupal.settings.social_feeds.length) {
        $.each(Drupal.settings.social_feeds, function (key, value) {

          $("#" + value['id'], context).once('social-feeds', function () {
            socialfeed(value);
          });

        });
      }

      // Tweet actions neccesary.
      !function (d,s,id) {
        var js,fjs = d.getElementsByTagName(s)[0],p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) {
          js = d.createElement(s);
          js.id = id;
          js.src = p + '://platform.twitter.com/widgets.js';
          fjs.parentNode.insertBefore(js,fjs);
        }
      }(document, 'script', 'twitter-wjs');
      // @code
      // @NOTYET
      // !function(d,s,id) {
      //   var js, fjs = d.getElementsByTagName(s)[0];
      //   if (d.getElementById(id)) return;
      //   js = d.createElement(s); js.id = id;
      //   js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.4";
      //   fjs.parentNode.insertBefore(js, fjs);
      // }(document, 'script', 'facebook-jssdk');
      // @endcode
      function socialfeed(value) {

        var $selector = $("#" + value['id']);

        $.getJSON(value.url, function (data) {
          var items = [];
          $.each(data, function (key, val) {
            items.push("<li class='" + val.type_id + "'>" + val.html + "</li>");
          });

          $selector.html($("<ul/>", {
            "class": "social-feeds-list",
            html: items.join("")
          }));

          setTimeout(function () {
            socialfeed(value);
          }, 60000);

        });
      }

    }
  };
})(jQuery, Drupal, this, this.document);
