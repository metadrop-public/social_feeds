<?php

/**
 * @file
 * Template file.
 */
?>
<div class="feed-content">
  <div class="feed-ago"><?php print social_feeds_time_elapsed_string($feed['date']); ?></div>
  <div class="feed-user-link"><?php print $user_link; ?></div>
  <div class="feed-user-name"><span class="<?php print $feed['type_class']?>"></span><?php print $feed['user_name']; ?></div>
  <div class="feed-text"><?php print $feed['text']; ?></div>
  <div class="feed-media"><?php print $media; ?></div>
  <div class="feed-actions"><?php print $feed_actions; ?></div>
  <div class="feed-read-more-link"><?php print $read_more_link; ?></div>
  <!-- @NOTYET <div class="fb-like" data-layout="button"></div> -->
</div>
